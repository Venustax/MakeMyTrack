package makemytrack;

public class Effects {

    private static Effects _oInstance;

    private Effects() {
    }

    public static synchronized Effects getInstance() {
        if (Effects._oInstance == null) {
            Effects._oInstance = new Effects();
        }
        return Effects._oInstance;
    }
    
    public String[] getEffectListForMixer(){
        /**
         * @ToDo an echte gegebenheit anpassen, momentan nur für die Ansicht unter UserInterface statisch
         */ 
        String[] aEffects = { "none", "Equalizer", "Reverb", "Distortion" };
        
        return aEffects;
    }
}
