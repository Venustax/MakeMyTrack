package makemytrack;

public class Config {

    /**
     * Einstellungen
     */
    double fProjectVersion = 0.1;
    int iNumberOfMixerChannels = 16;
    int iNumberOfMixerChannelEffects = 3;
    String sProjectName = "Venustax.net|MakeMyTrack v" + this.fProjectVersion;

    /**
     * Klassen-Variablen und Aufruf
     */
    private static Config _oInstance;

    private Config() {
    }

    public static synchronized Config getInstance() {
        if (Config._oInstance == null) {
            Config._oInstance = new Config();
        }
        return Config._oInstance;
    }
}
