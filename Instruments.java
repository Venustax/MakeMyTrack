package makemytrack;

public class Instruments {

    private static Instruments _oInstance;

    private Instruments() {
    }

    public static synchronized Instruments getInstance() {
        if (Instruments._oInstance == null) {
            Instruments._oInstance = new Instruments();
        }
        return Instruments._oInstance;
    }

    public String[] getInstrumentListForNewPanel() {
        /**
         * @ToDo an echte gegebenheit anpassen, momentan nur für die Ansicht
         * unter UserInterface statisch
         */
        String[] aInstruments = {"none", "Audio File", "Plugins..."};

        return aInstruments;
    }

}
