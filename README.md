# MakeMyTrack
MakeMyTrack, and what is your Track?

MakeMyTrack is an open-source music production software. The suite is designed and developed by Venustax.net (representing years of software developing) in a collaboration with Dream-Tunez.de (representing years of music creation in different styles). MakeMyTrack is for those who are loving collaborations around the world and works on every machine which supports JAVA.

MakeMyTrack helps you to exchange your project files, because it uses standard import / export files. Team sessions are enabled by sharing your screen and audio output with accepted partners.

The strength of this software is that it is open-source . This makes it possible that the community supports, tests and bugfixes through github and make comments with there great ideas for new features.

Copyright by MakeMyTrack (http://makemytrack.net) | All Rights Reserved.



### Changelog

#### 0.1 (Coming Soon)
- User-Interface / Window-Core-Layout
