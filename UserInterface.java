package makemytrack;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JTabbedPane;
import javax.swing.JComponent;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.JComboBox;

public class UserInterface {

    private JFrame _oWindow;
    private static UserInterface _oInstance;

    private UserInterface() {
        this._oWindow = new JFrame();
    }

    public static synchronized UserInterface getInstance() {
        if (UserInterface._oInstance == null) {
            UserInterface._oInstance = new UserInterface();
        }
        return UserInterface._oInstance;
    }

    public void setWindow() {
        this._setWindowOptions();
        this._setControlPanel();
        this._setTabs();

        /**
         * Fenster Maximieren
         */
        this._oWindow.setExtendedState(this._oWindow.getExtendedState() | JFrame.MAXIMIZED_BOTH);
    }

    private void _setWindowOptions() {
        Config oConfig = Config.getInstance();

        this._oWindow.setTitle(oConfig.sProjectName);
        this._oWindow.setSize(650, 500);
        this._oWindow.setLocationRelativeTo(null);
        this._oWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this._oWindow.setVisible(true);
    }

    private void _setControlPanel() {
        JPanel oControlPanel = new JPanel();

        oControlPanel.setLayout(new BorderLayout());

        oControlPanel.add(this._getSuiteHandlePanel(), BorderLayout.WEST);
        oControlPanel.add(this._getFileHandlePanel(), BorderLayout.EAST);

        this._oWindow.add(oControlPanel, BorderLayout.NORTH);
    }

    private JPanel _getSuiteHandlePanel() {
        JPanel oSuiteHandle = new JPanel();

        JButton bPlay = new JButton("Play / Stop");
        oSuiteHandle.add(bPlay);

        return oSuiteHandle;
    }

    private JPanel _getFileHandlePanel() {
        JPanel oFileHandlePanel = new JPanel();

        JButton bNew = new JButton("New");
        oFileHandlePanel.add(bNew);

        JButton bLoad = new JButton("Load");
        oFileHandlePanel.add(bLoad);

        JButton bSave = new JButton("Save");
        oFileHandlePanel.add(bSave);

        return oFileHandlePanel;
    }

    private void _setTabs() {
        JTabbedPane oTabs = new JTabbedPane();

        JComponent oTimeLine = this._createTimeLinePanel();
        oTabs.addTab("Timeline", oTimeLine);

        JComponent oInstruments = this._createInstrumentsPanel();
        oTabs.addTab("Instruments", oInstruments);

        JComponent oMixer = this._createMixerPanel();
        oTabs.addTab("Mixer", oMixer);

        this._oWindow.add(oTabs);
    }

    private JPanel _createTimeLinePanel() {
        JPanel oTimeLinePanel = new JPanel();

        return oTimeLinePanel;
    }

    private JPanel _createInstrumentsPanel() {
        JPanel oInstruments = new JPanel();

        oInstruments.add(this._createInstrumentPanel());

        return oInstruments;
    }

    private JPanel _createMixerPanel() {
        Config oConfig = Config.getInstance();

        JPanel oMixer = new JPanel();
        oMixer.setLayout(new GridLayout(0, 1));

        for (int i = 1; i <= oConfig.iNumberOfMixerChannels; i++) {
            oMixer.add(this._createMixerChannelPanel(i));
        }

        return oMixer;
    }

    private JPanel _createMixerChannelPanel(int iNumberOfChannel) {
        JPanel oChannel = new JPanel();
        Config oConfig = Config.getInstance();
        Effects oEffects = Effects.getInstance();

        JLabel lLabel = new JLabel("Channel " + iNumberOfChannel);
        oChannel.add(lLabel);

        JSlider sSlider = new JSlider(JSlider.HORIZONTAL, 0, 100, 100);
        oChannel.add(sSlider);

        for (int i = 1; i <= oConfig.iNumberOfMixerChannelEffects; i++) {
            JComboBox cEffects = new JComboBox(oEffects.getEffectListForMixer());
            oChannel.add(cEffects);

            JButton bEdit = new JButton("Edit");
            oChannel.add(bEdit);
        }

        return oChannel;
    }

    private JPanel _createInstrumentPanel() {
        JPanel oChannel = new JPanel();
        Instruments oInstruments = Instruments.getInstance();

        JComboBox cInstruments = new JComboBox(oInstruments.getInstrumentListForNewPanel());
        oChannel.add(cInstruments);

        JSlider sSlider = new JSlider(JSlider.HORIZONTAL, 0, 100, 100);
        oChannel.add(sSlider);

        JButton bEdit = new JButton("Edit");
        oChannel.add(bEdit);

        return oChannel;
    }

}
